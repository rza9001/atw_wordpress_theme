			<!-- footer -->
			<div class="clear"></div>
		</div>
		<!-- /wrapper -->
			<footer class="footer clear" role="contentinfo">
				<div class="wrapper">
					<div class="footer-widget">
						<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-2')) ?>
					</div>
				</div>
				<div class="clear"></div>
				<div class="copyright">
					<p>Copyright <?php echo date('Y'); ?> - All rights reserved </p>
				</div>
			</footer>
			<!-- /footer -->



		<?php wp_footer(); ?>

	</body>
</html>
