<?php 
/*
* Template Name: Tours
*/

get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>

    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

      <h2><span><?php the_title(); ?></span></h2>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<!-- Query posts dari Custom Post Type UI (CPT UI). -->
				<?php $args = array(
					'post_type' => 'tours2', // Post Type Name from CPT UI
					'posts_per_page' => -1, // Query all of the posts
					'orderby' => 'title',
					'order' => 'ASC'
				); ?>
				<ul class="tours">
					<?php $tours = new WP_Query($args); while($tours->have_posts()): $tours->the_post(); ?>
						<li class="grid2-4">
							<div class="featured-tour">
								<?php the_post_thumbnail('featuredTour'); ?>
								<a href="<?php the_permalink(); ?>" class="more-info">
									<!-- Load an image from the img folder -->
									<img src="<?php echo get_template_directory_uri(); ?>/img/moreinfo.png" alt="">
								</a>
							</div>
							<!-- featured-tour end -->

							<h3>
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?>
								</a>
							</h3>

							<!-- Print date from Advanced custom fields plugins -->
							<?php
								// Dates
								$format = 'd F, Y';
								$date = strtotime(get_field('leaving_date')); // leaving_date = Field Name dari Custom Fields plugins
								$leavingDate = date_i18n($format, $date);

								$returnDate = strtotime(get_field('returning_date')); // returning_date = Field Name dari Custom Fields plugins
								$returningDate = date_i18n($format, $returnDate);
							?>

							<div class="date-price clear">
								<p class="date"><?php echo $leavingDate . ' - ' . $returningDate; ?></p>
								<p class="price"><?php the_field('price'); // Get the price from Advance Custom Fields Plugin Name ?></p>
							</div>
							<!-- date-price -->

							<div class="tour-description">
								<?php the_field('small_description'); // Get the small_description from Advance Custom Fields Plugin Name ?>
							</div>
						</li>
					<?php endwhile; wp_reset_postdata();  ?>
				</ul>
				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>

