(function($, root, undefined) {
  $(function() {
    "use strict";

    // Responsive menu using SLicknav
    jQuery('header nav ul').slicknav();

    // Fluidbox Plugin
    // Added data-fluidbox attribute
    jQuery(".gallery a").each(function() {
      jQuery(this).attr({ "data-fluidbox": "" });
    });

    if (jQuery("[data-fluidbox]").length > 0) {
      jQuery("[data-fluidbox]").fluidbox();
    }


    // BX Slider
    jQuery('ul.slider').bxSlider({
      pager: false,
      auto: true
    });
  });
})(jQuery, this);